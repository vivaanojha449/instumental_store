import colors from 'colors';
import dotenv from 'dotenv';
import express from 'express';
import path from 'path';
import connectDB from './config/db.js';
import { errorHandler } from './middlewares/errorMiddleware.js';
import orderRoutes from './routes/orderRoutes.js';
import productRoutes from './routes/productRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import userRoutes from './routes/userRoutes.js';
import cors from 'cors';

dotenv.config();

connectDB();

const app = express();
app.use(express.json()); // Request Body Parsing

app.use(cors({
	origin: 'https://instrumental-store.onrender.com', // Replace with your actual frontend URL
	methods: ['GET', 'POST', 'PUT', 'DELETE'], // Specify the allowed methods
	credentials: true, // Allow credentials if needed
  }));


app.use('/api/products', productRoutes);
app.use('/api/users', userRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/uploads', uploadRoutes);



// Create a static folder
const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

if (process.env.NODE_ENV === 'production') {
	app.get('/', (req, res) => {
	  res.send('API is running...');
	});
  }

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
	console.log(
		`Server running in ${process.env.NODE_ENV} mode, on port ${PORT}.`.yellow
			.bold
	);
});
