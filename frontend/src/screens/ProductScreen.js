import {
    Button,
    Flex,
    Grid,
    Heading,
    Image,
    Select,
    Text,
  } from '@chakra-ui/react';
  import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
  import Rating from '../components/Rating';
  import { useEffect, useState } from 'react';
  import { useDispatch, useSelector } from 'react-redux';
  import Loader from '../components/Loader';
  import Message from '../components/Message';
  import { listProductDetails } from '../actions/productActions';
  
  const ProductScreen = () => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const navigate = useNavigate();
  
    const [qty, setQty] = useState(1);
  
    const productDetails = useSelector((state) => state.productDetails);
    const { loading, error, product } = productDetails;
  
    useEffect(() => {
      dispatch(listProductDetails(id));
    }, [id, dispatch]);
  
    const addToCartHandler = () => {
      navigate(`/cart/${id}?qty=${qty}`);
    };
  
  
    return (
      <>
        <Flex direction='column' px={{ base: '4', md: '0' }}>
          <Button as={RouterLink} to='/' colorScheme='gray' mb='5'>
            Go Back
          </Button>
        </Flex>
  
        {loading ? (
          <Loader />
        ) : error ? (
          <Message type='error'>{error}</Message>
        ) : (
          <Grid
            templateColumns={{ base: '1fr', md: '5fr 4fr 3fr' }}
            gap={{ base: '6', md: '10' }}
            px={{ base: '4', md: '0' }}
          >
            {/* Column 1 */}
            <Image src={product.image} alt={product.name} borderRadius='md' />
  
            {/* Column 2 */}
            <Flex direction='column'>
              <Heading as='h5' fontSize='sm' color='gray.500' mb='2'>
                {product.brand}
              </Heading>
              <Heading as='h2' fontSize={{ base: '2xl', md: '4xl' }} mb='4'>
                {product.name}
              </Heading>
  
              <Rating value={product.rating} text={`${product.numReviews} reviews`} />
  
              <Heading
                as='h3'
                fontSize={{ base: '2xl', md: '4xl' }}
                fontWeight='bold'
                color='teal.600'
                my='5'
              >
                ₹{product.price}
              </Heading>
              <Text fontSize='sm'>{product.description}</Text>
            </Flex>
  
            {/* Column 3 */}
            <Flex direction='column' alignItems='flex-start'>
              <Flex justifyContent='space-between' py='2' width='100%'>
                <Text fontSize='sm'>Price:</Text>
                <Text fontWeight='bold' fontSize='sm'>
                  ₹{product.price}
                </Text>
              </Flex>
              <Flex justifyContent='space-between' py='2' width='100%'>
                <Text fontSize='sm'>Status:</Text>
                <Text fontWeight='bold' fontSize='sm'>
                  {product.countInStock > 0 ? 'In Stock' : 'Not available'}
                </Text>
              </Flex>
  
              {product.countInStock > 0 && (
                <Flex justifyContent='space-between' py='2' width='100%'>
                  <Text fontSize='sm'>Qty:</Text>
                  <Select
                    value={qty}
                    onChange={(e) => setQty(e.target.value)}
                    width='50%'
                    fontSize='sm'
                  >
                    {[...Array(product.countInStock).keys()].map((i) => (
                      <option key={i + 1} value={i + 1}>
                        {i + 1}
                      </option>
                    ))}
                  </Select>
                </Flex>
              )}
  
              <Button
                bg='gray.800'
                colorScheme='teal'
                my='2'
                textTransform='uppercase'
                letterSpacing='wide'
                onClick={addToCartHandler}
                isDisabled={product.countInStock === 0}
                fontSize={{ base: 'sm', md: 'md' }}
                width='100%'
              >
                Add to Cart
              </Button>
            </Flex>
          </Grid>
        )}
      </>
    );
  };
  
  export default ProductScreen;
  