import { Box, Grid, Heading, Slider, SliderTrack, SliderFilledTrack, SliderThumb, Accordion, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon } from '@chakra-ui/react';
import ProductCard from '../components/ProductCard';
import { useEffect } from 'react';
import { listProducts } from '../actions/productActions';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/Loader';
import Message from '../components/Message';

const HomeScreen = () => {
    const dispatch = useDispatch();
    const productList = useSelector((state) => state.productList);
    const { loading, error, products } = productList;

    useEffect(() => {
        dispatch(listProducts());
    }, [dispatch]);

    const guitarTypes = ['Acoustic', 'Electric', 'Classical', 'Bass'];

    const accessories = [
        {
            category: 'Guitar Accessories',
            items: ['Guitar Strings', 'Cables & Connectors', 'Guitar Straps', 'Bags & Cases', 'Guitar Tuners']
        },
        {
            category: 'Drum Accessories',
            items: ['Drum Sticks', 'Drum Heads', 'Drum Thrones', 'Drum Cases']
        },
        {
            category: 'Violins',
            items: ['Acoustic Violins', 'Cello', 'Electric']
        },
        {
            category: 'Ukulele',
            items: ['Baritone', 'Concert', 'Demi Cutaway', 'Tenor', 'Wanderers Ukulele']
        },
        {
            category: 'Guitar Amplifiers',
            items: ['Bass', 'Drum Amplifier', 'Electric']
        },
        {
            category: 'Percussions',
            items: ['Cajons', 'Kalimba', 'Wambooka Diamond Dry and Wet Darbuka']
        }
    ];

    return (
        <>
            <Grid templateColumns={['1fr', '1fr 3fr']} gap='8'>
                <Box>
                    <Box mb='8'>
                        <Heading as='h3' fontSize={['xl', 'xl', '2xl']} mb='4'>Categories</Heading>
                        <Box textAlign='center' >
                            <Slider aria-label='price-slider' defaultValue={500} max={50000} position={'fixed'}>
                                <SliderTrack bg='red.100'>
                                    <SliderFilledTrack bg='tomato' />
                                </SliderTrack>
                                <SliderThumb boxSize={6}>
                                    <Box color='tomato' />
                                </SliderThumb>
                            </Slider>
                            <Box>Price: ₹0 - ₹50,000</Box>
                        </Box>
                        <Heading as='h4' fontSize='md' mt='6' mb='2'>Music Instrument Accessories</Heading>
                        <Accordion allowToggle>
                            {accessories.map((category, index) => (
                                <AccordionItem key={index}>
                                    <AccordionButton>
                                        <Box flex='1' textAlign='left'>{category.category}</Box>
                                        <AccordionIcon />
                                    </AccordionButton>
                                    <AccordionPanel pb={4}>
                                        <ul>
                                            {category.items.map((item, idx) => (
                                                <li key={idx}>{item}</li>
                                            ))}
                                        </ul>
                                    </AccordionPanel>
                                </AccordionItem>
                            ))}
                        </Accordion>
                    </Box>
                    <Heading as='h3' fontSize={['xl', 'xl', '2xl']} mb='4'>Guitars</Heading>
                    <Grid templateColumns='1fr' gap='4'>
                        {guitarTypes.map((type) => (
                            <Box key={type} bg='gray.100' p='4'>{type}</Box>
                        ))}
                    </Grid>
                </Box>
                <Box>
                    <Heading as='h2' mb='8' fontSize={['2xl', '3xl']}>Newest Products</Heading>
                    {loading ? (
                        <Loader />
                    ) : error ? (
                        <Message type='error'>{error}</Message>
                    ) : (
                        <Grid templateColumns={['1fr', '1fr 1fr 1fr', '1fr 1fr 1fr']} gap='8'>
                            {products.map((prod) => (
                                <ProductCard key={prod._id} product={prod} />
                            ))}
                        </Grid>
                    )}
                </Box>
            </Grid>
        </>
    );
}

export default HomeScreen;
