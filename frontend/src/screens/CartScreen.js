import {
	Box,
	Button,
	Flex,
	Grid,
	Heading,
	Icon,
	Image,
	Link,
	Select,
	Text,
	useMediaQuery,
} from '@chakra-ui/react';
import { useEffect } from 'react';
import { IoTrashBinSharp } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import {
	Link as RouterLink,
	useNavigate,
	useParams,
	useSearchParams,
} from 'react-router-dom';
import { addToCart, removeFromCart } from '../actions/cartActions';
import Message from '../components/Message';

const CartScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const { id } = useParams();
	const [searchParams] = useSearchParams();
	let qty = searchParams.get('qty');

	const cart = useSelector((state) => state.cart);
	const { cartItems } = cart;

	useEffect(() => {
		if (id) {
			dispatch(addToCart(id, +qty));
		}
	}, [dispatch, id, qty]);

	const removeFromCartHandler = (id) => {
		dispatch(removeFromCart(id));
	};

	const checkoutHandler = () => {
		navigate(`/login?redirect=/shipping`);
	};

	const [isLargerThan768] = useMediaQuery('(min-width: 768px)');

	return (
		<Grid templateColumns={{ base: '1fr', md: '4fr 2fr' }} gap='8' px={{ base: '2', md: '0' }}>
			<Box>
				<Heading mb='8' fontSize='2xl'>
					Shopping Cart
				</Heading>
				{cartItems.length === 0 ? (
					<Message>
						Your cart is empty.{' '}
						<Link as={RouterLink} to='/'>
							Go back
						</Link>
					</Message>
				) : (
					<Flex direction='column'>
						{cartItems.map((item) => (
							<Flex
								key={item.product}
								alignItems='center'
								justifyContent='space-between'
								borderBottom='1px'
								borderColor='gray.200'
								py='4'
								px='2'
								rounded='lg'
								_hover={{ bgColor: 'gray.50' }}
								mb='4'
								flexDir={{ base: 'column', md: 'row' }}
							>
								{/* Product Image */}
								<Image
									src={item.image}
									alt={item.name}
									borderRadius='lg'
									boxSize={{ base: '24', md: '32' }} // Adjusted size for mobile and desktop
									objectFit='cover'
									mr={{ base: '0', md: '4' }} // Margin for image alignment
									mb={{ base: '4', md: '0' }} // Margin for mobile view
								/>

								<Flex direction='column' flex='1' ml={{ base: '0', md: '4' }}>
									{/* Product Name */}
									<Text fontWeight='semibold' fontSize={{ base: 'xs', md: 'sm' }} mb='1'>
										<Link as={RouterLink} to={`/product/${item.product}`}>
											{item.name}
										</Link>
									</Text>

									{/* Product Price */}
									<Text fontWeight='semibold' fontSize={{ base: 'xs', md: 'sm' }} mb={{ base: '2', md: '0' }}>
										₹{item.price}
									</Text>
								</Flex>

								<Flex alignItems='center'>
									{/* Quantity Select Box */}
									<Select
										value={item.qty}
										onChange={(e) => dispatch(addToCart(item.product, +e.target.value))}
										width={{ base: '12', md: '20' }}
										mr={{ base: '2', md: '4' }}
									>
										{[...Array(item.countInStock).keys()].map((i) => (
											<option key={i + 1}>{i + 1}</option>
										))}
									</Select>

									{/* Delete Button */}
									<Button
										type='button'
										colorScheme='red'
										onClick={() => removeFromCartHandler(item.product)}
									>
										<Icon as={IoTrashBinSharp} />
									</Button>
								</Flex>
							</Flex>
						))}
					</Flex>
				)}
			</Box>

			{/* Summary Column */}
			<Box>
				<Heading as='h2' fontSize='xl' mb='4'>
					Subtotal ({cartItems.reduce((acc, currVal) => acc + currVal.qty, 0)} items)
				</Heading>
				<Flex direction='column' bg='gray.100' rounded='md' p='4' mb='4'>
					<Text fontWeight='semibold' fontSize='lg' mb='2'>
						Total: ₹
						{cartItems
							.reduce((acc, currVal) => acc + currVal.price * currVal.qty, 0)
							.toFixed(2)}
					</Text>
					<Button
						type='button'
						size='lg'
						colorScheme='teal'
						bgColor='teal.500'
						_hover={{ bgColor: 'teal.600' }}
						onClick={checkoutHandler}
					>
						Proceed to checkout
					</Button>
				</Flex>
			</Box>
		</Grid>
	);
};

export default CartScreen;
