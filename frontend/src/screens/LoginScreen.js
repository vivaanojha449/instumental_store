import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Input,
	Link,
	Text,
	useMediaQuery,
	useDisclosure,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Link as RouterLink,
	useNavigate,
	useSearchParams,
} from 'react-router-dom';
import { login } from '../actions/userActions';
import FormContainer from '../components/FormContainer';
import Message from '../components/Message';

const LoginScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const { isOpen, onOpen, onClose } = useDisclosure();

	let [searchParam] = useSearchParams();
	let redirect = searchParam.get('redirect') || '/';

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const userLogin = useSelector((state) => state.userLogin);
	const { loading, error, userInfo } = userLogin;

	useEffect(() => {
		if (userInfo) {
			navigate(redirect);
		}
	}, [navigate, userInfo, redirect]);

	const submitHandler = (e) => {
		e.preventDefault();
		dispatch(login(email, password));
	};

	const [isLargerThan768] = useMediaQuery('(min-width: 768px)');

	return (
		<Flex
			w='full'
			alignItems='center'
			justifyContent='center'
			py='5'
			minH='100vh'
			bgGradient='linear(to-r, teal.400, blue.500)'
		>
			<FormContainer maxW='md' p={isLargerThan768 ? '8' : '4'} bg='white' borderRadius='md' boxShadow='lg'>
				<Heading as='h1' mb='8' fontSize='3xl' textAlign='center' color='teal.600'>
					Login
				</Heading>

				{error && <Message type='error'>{error}</Message>}

				<form onSubmit={submitHandler}>
					<FormControl id='email'>
						<FormLabel htmlFor='email'>Email address</FormLabel>
						<Input
							id='email'
							type='email'
							placeholder='username@domain.com'
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
					</FormControl>

					<FormControl id='password' mt='4'>
						<FormLabel htmlFor='password'>Password</FormLabel>
						<Input
							id='password'
							type='password'
							placeholder='************'
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
					</FormControl>

					<Button type='submit' colorScheme='teal' mt='6' width='full' isLoading={loading}>
						Login
					</Button>
				</form>

				<Flex pt='6' justifyContent='center'>
					<Text fontWeight='semibold' color='teal.600'>
						New Customer?{' '}
						<Link as={RouterLink} to='/register' color='teal.600'>
							Click here to register
						</Link>
					</Text>
				</Flex>
			</FormContainer>
		</Flex>
	);
};

export default LoginScreen;
