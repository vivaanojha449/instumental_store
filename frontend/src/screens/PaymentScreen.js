import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Radio,
	RadioGroup,
	useMediaQuery,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { savePaymentMethod } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';

const PaymentScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const cart = useSelector((state) => state.cart);
	const { shippingAddress, paymentMethod } = cart;

	const [paymentMethodRadio, setPaymentMethodRadio] = useState(paymentMethod || 'paypal');

	useEffect(() => {
		if (!shippingAddress) {
			navigate('/shipping');
		}
	}, [navigate, shippingAddress]);

	const submitHandler = (e) => {
		e.preventDefault();
		dispatch(savePaymentMethod(paymentMethodRadio));
		navigate('/placeorder');
	};

	const [isLargerThan768] = useMediaQuery('(min-width: 768px)');

	return (
		<Flex
			w='full'
			alignItems='center'
			justifyContent='center'
			py='5'
			px={{ base: '2', md: '0' }}
		>
			<FormContainer>
				<CheckoutSteps step1 step2 step3 />

				<Heading as='h2' mb='8' fontSize={{ base: '2xl', md: '3xl' }}>
					Payment Method
				</Heading>

				<form onSubmit={submitHandler}>
					<FormControl as='fieldset'>
						<FormLabel as='legend'>Select Method</FormLabel>
						<RadioGroup
							value={paymentMethodRadio}
							onChange={setPaymentMethodRadio}
							direction={isLargerThan768 ? 'row' : 'column'}
						>
							<Radio value='paypal'>PayPal or Credit/Debit Card</Radio>
						</RadioGroup>
					</FormControl>

					<Button
						type='submit'
						colorScheme='teal'
						mt={{ base: '4', md: '8' }}
						w='full'
						fontSize='lg'
					>
						Continue
					</Button>
				</form>
			</FormContainer>
		</Flex>
	);
};

export default PaymentScreen;
