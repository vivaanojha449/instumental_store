import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Input,
	Select,
	Spacer,
	useMediaQuery,
} from '@chakra-ui/react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { saveShippingAddress } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';
import countries from '../data/countries';

const ShippingScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const cart = useSelector((state) => state.cart);
	const { shippingAddress } = cart;

	const [address, setAddress] = useState(shippingAddress.address || '');
	const [city, setCity] = useState(shippingAddress.city || '');
	const [postalCode, setPostalCode] = useState(
		shippingAddress.postalCode || ''
	);
	const [country, setCountry] = useState(shippingAddress.country || '');

	const submitHandler = (e) => {
		e.preventDefault();
		dispatch(saveShippingAddress({ address, city, postalCode, country }));
		navigate('/payment');
	};

	const [isSmallerThan768] = useMediaQuery('(max-width: 767px)');

	return (
		<Flex w='full' alignItems='center' justifyContent='center' py={{ base: '3', md: '5' }}>
			<FormContainer>
				<Heading as='h2' mb='8' fontSize={{ base: '2xl', md: '3xl' }}>
					Shipping
				</Heading>
				<CheckoutSteps step1 step2 />

				<form onSubmit={submitHandler}>
					{/* Address */}
					<FormControl id='address' mb='4'>
						<FormLabel htmlFor='address'>Address</FormLabel>
						<Input
							id='address'
							type='text'
							placeholder='Your Address'
							value={address}
							onChange={(e) => setAddress(e.target.value)}
						/>
					</FormControl>

					{/* City */}
					<FormControl id='city' mb='4'>
						<FormLabel htmlFor='city'>City</FormLabel>
						<Input
							id='city'
							type='text'
							placeholder='Your City'
							value={city}
							onChange={(e) => setCity(e.target.value)}
						/>
					</FormControl>

					{/* Postal Code */}
					<FormControl id='postalCode' mb='4'>
						<FormLabel htmlFor='postalCode'>Postal Code</FormLabel>
						<Input
							id='postalCode'
							type='text'
							placeholder='Your Postal Code'
							value={postalCode}
							onChange={(e) => setPostalCode(e.target.value)}
						/>
					</FormControl>

					{/* Country */}
					<FormControl id='country' mb='4'>
						<FormLabel htmlFor='country'>Country</FormLabel>
						<Select
							value={country}
							onChange={(e) => setCountry(e.target.value)}
							placeholder='Select Country'>
							{countries.map((country) => (
								<option key={country} value={country}>
									{country}
								</option>
							))}
						</Select>
					</FormControl>

					<Button type='submit' colorScheme='teal' mt='4' w='100%'>
						Continue
					</Button>
				</form>
			</FormContainer>
		</Flex>
	);
};

export default ShippingScreen;
