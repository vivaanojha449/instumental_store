import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Input,
	Link,
	Spacer,
	Text,
	useMediaQuery,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Link as RouterLink,
	useNavigate,
	useSearchParams,
} from 'react-router-dom';
import { register } from '../actions/userActions';
import FormContainer from '../components/FormContainer';
import Message from '../components/Message';

const RegisterScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	let [searchParams] = useSearchParams();
	let redirect = searchParams.get('redirect') || '/';

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [message, setMessage] = useState(null);

	const userRegister = useSelector((state) => state.userRegister);
	const { loading, error, userInfo } = userRegister;

	useEffect(() => {
		if (userInfo) {
			navigate(redirect);
		}
	}, [navigate, userInfo, redirect]);

	const submitHandler = (e) => {
		e.preventDefault();
		if (password !== confirmPassword) {
			setMessage('Passwords do not match');
		} else {
			dispatch(register(name, email, password));
		}
	};

	const [isSmallerThan768] = useMediaQuery('(max-width: 767px)');

	return (
		<Flex w='full' alignItems='center' justifyContent='center' py={{ base: '3', md: '5' }}>
			<FormContainer>
				<Heading as='h1' mb='8' fontSize={{ base: '2xl', md: '3xl' }}>
					Register
				</Heading>

				{error && <Message type='error'>{error}</Message>}
				{message && <Message type='error'>{message}</Message>}

				<form onSubmit={submitHandler}>
					<FormControl id='name' mb='4'>
						<FormLabel htmlFor='name'>Your Name</FormLabel>
						<Input
							id='name'
							type='text'
							placeholder='Your full name'
							value={name}
							onChange={(e) => setName(e.target.value)}
						/>
					</FormControl>

					<FormControl id='email' mb='4'>
						<FormLabel htmlFor='email'>Email address</FormLabel>
						<Input
							id='email'
							type='email'
							placeholder='username@domain.com'
							value={email}
							onChange={(e) => setEmail(e.target.value)}
						/>
					</FormControl>

					<FormControl id='password' mb='4'>
						<FormLabel htmlFor='password'>Password</FormLabel>
						<Input
							id='password'
							type='password'
							placeholder='************'
							value={password}
							onChange={(e) => setPassword(e.target.value)}
						/>
					</FormControl>

					<FormControl id='confirmPassword' mb='4'>
						<FormLabel htmlFor='confirmPassword'>Confirm Password</FormLabel>
						<Input
							id='confirmPassword'
							type='password'
							placeholder='************'
							value={confirmPassword}
							onChange={(e) => setConfirmPassword(e.target.value)}
						/>
					</FormControl>

					<Button type='submit' colorScheme='teal' mt='4' isLoading={loading} w='100%'>
						Register
					</Button>
				</form>

				<Flex pt='6' justifyContent='center'>
					<Text fontSize='sm' fontWeight='semibold'>
						Already a Customer?{' '}
						<Link as={RouterLink} to='/login'>
							Click here to login
						</Link>
					</Text>
				</Flex>
			</FormContainer>
		</Flex>
	);
};

export default RegisterScreen;
