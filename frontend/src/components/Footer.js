import { Box, Flex, Text } from '@chakra-ui/react';

const Footer = () => {
    return (
        <Flex
        as='footer'
        justifyContent='center'
        py='5'
        bgColor={'#222225'}
        color='white'
        >
            <Box>
                <Text color={'#bbb'} >
                    Copyright {new Date().getFullYear()}. <Text color={'#fefefe'} fontSize={'xl'} display={'inline-block'}>Instrumental Store.</Text> All Rights Reserved.
                </Text>
                <Text color={'#777'} pl={'8'}>Capturing melodies, one instrument at a time.</Text>
            </Box>
        </Flex>
    );
}

export default Footer;
