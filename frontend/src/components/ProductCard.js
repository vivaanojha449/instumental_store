import React from 'react'
import { Link as RouterLink } from 'react-router-dom';
import { Box, Flex, Heading, Image, Link, Text } from '@chakra-ui/react'
import Rating from '../components/Rating';

//doubt product id coming from
const ProductCard = ({ product }) => {
    return (
        <Link as={RouterLink} to={`/product/${product._id}`} _hover={{ textDecor: 'none' }}> 
            <Box borderRadius={'lg'} bgColor={'white'} _hover={{ shadow: 'md' }} h={'100%'}>
                <Image
                    src={product.image}
                    alt={product.name}
                    w='full'
                    h='430px'
                    objectFit={'cover'}
                />
                <Flex py={'5'} px={'4'} direction={'column'} justifyContent={'space-between'} >
                    <Heading as='h4' fontSize={'lg'} mb={'3'}>
                        {product.name}
                    </Heading>
                    <Flex alignItems={'center'} justifyContent={'space-between'} >
                        <Rating value={product.Rating} />
                        <Text fontSize={'2xl'} fontWeight={'bold'} color='blue.600'>
                            ₹{product.price}
                        </Text>
                    </Flex>
                </Flex>

            </Box>
        </Link>
    )
}

export default ProductCard