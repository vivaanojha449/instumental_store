import {
	Box,
	Button,
	Flex,
	Heading,
	Icon,
	Link,
	Menu,
	MenuButton,
	MenuItem,
	MenuList,
    Input, InputGroup, InputLeftElement, Text
} from '@chakra-ui/react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { HiOutlineMenuAlt3, HiShoppingBag, HiUser, HiSearch } from 'react-icons/hi';
import HeaderMenuItem from '../components/HeaderMenuItem';
import { useDispatch, useSelector } from 'react-redux';
import { IoChevronDown } from 'react-icons/io5';
import { logout } from '../actions/userActions';


const ShoppingCartSummary = ({ totalItems, totalPrice }) => (
    <Box ml="4" display="flex" alignItems="center">
        <Icon as={HiShoppingBag} color="white" boxSize={6} />
        <Text color="white" ml="2">{totalItems} Items | ₹{totalPrice}</Text>
    </Box>
);

const Header = () => {
    const dispatch = useDispatch();
	const navigate = useNavigate();

    const [open, setOpen] = useState(false);

    const userLogin = useSelector((state) => state.userLogin);
	const { userInfo } = userLogin;

    const logoutHandler = () => {
		dispatch(logout());
		navigate('/login');
	};

    const totalItems = 5; // Replace with actual total items in the cart
    const totalPrice = 500; // Replace with actual total price of items in the cart

    return (
        <Flex
            as="header"
            align='center'
            justifyContent='space-between'
            wrap='wrap'
            py='6'
            px='6'
            bgColor='#222225'
            w='100%'
            pos='fixed'
            zIndex='999'
            top='0'
            left='0'
            >

            <Link as={RouterLink} to='/'>
                <Heading
                    as='h1'
                    color='#fefefe'
                    fontWeight='Bold'
                    size='md'
                    letterSpacing='wide'>
                    Instrumental Store
                </Heading>
            </Link>

            <Box
                display={{ base: 'block', md: 'none' }}
                onClick={() => setOpen(!open)}>
                <Icon as={HiOutlineMenuAlt3} color='white' w='6' h='6' />
            </Box>

            <InputGroup display={{ base: 'none', md: 'flex' }} maxW="sm" bgColor={'#fefefe'} borderRadius={'5'}>
                <InputLeftElement
                    pointerEvents="auto"
                    children={<Icon as={HiSearch} color="gray.300" />}
                />
                <Input type="text" placeholder="Search by products" />
            </InputGroup>

            <Box
                display={{ base: open ? 'block' : 'none', md: 'flex' }}
                width={{ base: 'full', md: 'auto' }}
				mt={{ base: '3', md: '0' }}>
                    

                    {userInfo ? (
					<Menu>
						<MenuButton
							as={Button}
							rightIcon={<IoChevronDown />}
							_hover={{ textDecor: 'none', opacity: '0.7' }}>
							{userInfo.name}
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/profile'>
								Profile
							</MenuItem>
                            {/* <MenuItem as={RouterLink} to='/admin/userlist'>
								UserList
							</MenuItem> */}
							<MenuItem onClick={logoutHandler}>Logout</MenuItem>
						</MenuList>
					</Menu>
				) : (
                    <HeaderMenuItem icon={HiUser} url='/login' label='Login/Register' />
				)}

{userInfo && userInfo.isAdmin && (
					<Menu>
						<MenuButton
							as={Button}
							marginLeft='10px'
							rightIcon={<IoChevronDown />}
							_hover={{ textDecor: 'none', opacity: '0.7' }}>
							Manage
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/admin/userlist'>
								All Users
							</MenuItem>
							<MenuItem as={RouterLink} to='/admin/productlist'>
								All Products
							</MenuItem>
							<MenuItem as={RouterLink} to='/admin/orderlist'>
								All Orders
							</MenuItem>
						</MenuList>
					</Menu>
				)}
                    
                <ShoppingCartSummary totalItems={totalItems} totalPrice={totalPrice} />
            </Box>
        </Flex>
    )
}

export default Header;
